package it.com.atlassian.rest.cors;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.Map;

import static com.atlassian.plugins.rest.common.security.CorsHeaders.*;
import static com.atlassian.plugins.rest.cors.SimpleCorsDefaults.*;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CorsCheckTest {
    @Test
    public void testSimpleGetWithOrigin() throws IOException {
        Map<String, String> headers = makeRequest("GET", ImmutableMap.<String, String>of(ORIGIN.value(), CREDENTIALS));
        assertEquals(CREDENTIALS, headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertEquals("true", headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertEquals("X-Response-Header", headers.get(ACCESS_CONTROL_EXPOSE_HEADERS.value()));
    }

    @Test
    public void testSimpleGetWithNoCredentialsOrigin() throws IOException {
        Map<String, String> headers = makeRequest("GET", ImmutableMap.<String, String>of(ORIGIN.value(), NO_CREDENTIALS));
        assertEquals(NO_CREDENTIALS, headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertEquals("X-Response-Header", headers.get(ACCESS_CONTROL_EXPOSE_HEADERS.value()));
    }

    @Test
    public void testPreflight() throws IOException {
        Map<String, String> headers = makeRequest("OPTIONS", ImmutableMap.<String, String>of(
                ORIGIN.value(), CREDENTIALS,
                ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT"
        ));
        assertEquals(CREDENTIALS, headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertEquals("true", headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertEquals(String.valueOf(60 * 60), headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertEquals("PUT", headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertEquals("X-Custom-Header", headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    @Test
    public void testPreflightWithWrongHeaders() throws Exception {
        Map<String, String> headers = makeRequest("OPTIONS", ImmutableMap.<String, String>of(
                ORIGIN.value(), NO_CREDENTIALS,
                ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT",
                ACCESS_CONTROL_REQUEST_HEADERS.value(), "X-Unexpected-Header"
        ));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertNull(headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    @Test
    public void testPreflightWithWrongOrigin() throws IOException {
        Map<String, String> headers = makeRequest("OPTIONS", ImmutableMap.<String, String>of(
                ORIGIN.value(), "http://www.invalid.com",
                ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT"
        ));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertNull(headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    @Test
    public void testPreflightWithWrongMethod() throws IOException {
        Map<String, String> headers = makeRequest(
                WebResourceFactory.anonymous().path("cors").path("none").getURI().toString(),
                "OPTIONS", ImmutableMap.<String, String>of(
                        ORIGIN.value(), CREDENTIALS,
                        ACCESS_CONTROL_REQUEST_METHOD.value(), "PUT"
                ));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_ORIGIN.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_CREDENTIALS.value()));
        assertNull(headers.get(ACCESS_CONTROL_MAX_AGE.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_METHODS.value()));
        assertNull(headers.get(ACCESS_CONTROL_ALLOW_HEADERS.value()));
    }

    private Map<String, String> makeRequest(String method, Map<String, String> headers) throws IOException {
        return makeRequest(WebResourceFactory.anonymous().path("cors").getURI().toString(), method, headers);
    }

    private Map<String, String> makeRequest(String uri, String method, Map<String, String> headers) throws IOException {
        URL url = new URL(uri);
        Socket socket = new Socket(url.getHost(), url.getPort());

        PrintWriter writer = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));
        writer.write(method + " " + url.getPath() + " HTTP/1.1\r\n");
        for (String key : headers.keySet()) {
            writer.write(key + ": " + headers.get(key) + "\r\n");
        }
        writer.write("Accept: text/html\r\n");
        writer.write("User-Agent: Me\r\n");
        writer.write("Connection: keep-alive\r\n");
        writer.write("Host: 127.0.0.1:" + url.getPort() + " \r\n");
        writer.write("\n\n");
        writer.flush();

        Map<String, String> responseHeaders = newHashMap();
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            if ("".equals(line)) {
                socket.close();
                break;
            }
            int pos = line.indexOf(':');
            if (pos > -1) {
                String key = line.substring(0, pos).trim();
                String value = line.substring(pos + 1).trim();
                responseHeaders.put(key, value);
            }
        }

        return responseHeaders;
    }
}
