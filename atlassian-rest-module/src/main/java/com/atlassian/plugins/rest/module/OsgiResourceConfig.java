package com.atlassian.plugins.rest.module;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.Module;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.rest.common.error.jersey.NotFoundExceptionMapper;
import com.atlassian.plugins.rest.common.error.jersey.ThrowableExceptionMapper;
import com.atlassian.plugins.rest.common.error.jersey.UncaughtExceptionEntityWriter;
import com.atlassian.plugins.rest.common.json.JacksonJsonProviderFactory;
import com.atlassian.plugins.rest.common.security.jersey.AdminOnlyResourceFilter;
import com.atlassian.plugins.rest.common.security.jersey.AuthorisationExceptionMapper;
import com.atlassian.plugins.rest.common.security.jersey.SecurityExceptionMapper;
import com.atlassian.plugins.rest.common.security.jersey.SysadminOnlyResourceFilter;
import com.atlassian.plugins.rest.module.filter.AcceptLanguageFilter;
import com.atlassian.plugins.rest.module.scanner.AnnotatedClassScanner;
import com.atlassian.plugins.rest.module.xml.XMLStreamReaderContextProvider;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of {@link ResourceConfig} that scans a given Osgi Bundle for Jersey resources ({#link Provider} and {@link Path}.
 */
class OsgiResourceConfig extends DefaultResourceConfig {
    private final Bundle bundle;

    @SuppressWarnings("unchecked")
    private final Set<Class<?>> classes = Sets.newHashSet(
            NotFoundExceptionMapper.class,
            AuthorisationExceptionMapper.class,
            SecurityExceptionMapper.class,
            ThrowableExceptionMapper.class,
            SysadminOnlyResourceFilter.class,
            AdminOnlyResourceFilter.class,
            UncaughtExceptionEntityWriter.class
    );

    private final Set<Object> instances;

    private Set<Class<?>> scannedClasses;
    private final String[] packages;

    OsgiResourceConfig(Bundle bundle,
                       Set<String> packages,
                       Collection<? extends ContainerRequestFilter> containerRequestFilters,
                       Collection<? extends ContainerResponseFilter> containerResponseFilters,
                       Collection<? extends ResourceFilterFactory> resourceFilterFactories,
                       Collection<? extends Module> modules,
                       Collection<?> providers) {
        this.packages = packages.toArray(new String[packages.size()]);
        this.bundle = requireNonNull(bundle);

        // adds "filters" to Jersey
        getProperties().put(PROPERTY_CONTAINER_REQUEST_FILTERS, Lists.newLinkedList(containerRequestFilters));
        getProperties().put(PROPERTY_CONTAINER_RESPONSE_FILTERS, Lists.newLinkedList(containerResponseFilters));
        getProperties().put(PROPERTY_RESOURCE_FILTER_FACTORIES, Lists.newLinkedList(resourceFilterFactories));

        this.instances = Sets.newHashSet(requireNonNull(providers));
        this.instances.add(new JacksonJsonProviderFactory().create(modules));
        this.instances.add(new XMLStreamReaderContextProvider(this));

        addInstancesClassesToClasses();
    }

    private void addInstancesClassesToClasses() {
        for (Object o : instances) {
            classes.add(o.getClass());
        }
    }

    @Override
    public synchronized Set<Class<?>> getClasses() {
        if (scannedClasses == null) {
            scannedClasses = scanForAnnotatedClasses();
            classes.addAll(scannedClasses);
        }

        return classes;
    }

    private Set<Class<?>> scanForAnnotatedClasses() {
        return new AnnotatedClassScanner(bundle, Provider.class, Path.class).scan(packages);
    }

    public Set<?> getInstances() {
        return Collections.unmodifiableSet(instances);
    }
}
