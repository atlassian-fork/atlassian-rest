package com.atlassian.plugins.rest.sample.entities;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Path("fruit")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
public class EntityResource {
    @POST
    @AnonymousAllowed
    public Response applesForOranges(final Orange orange, @Context HttpHeaders headers) {
        return Response.ok(new Apple("apple-" + orange.getName(),
                toString(headers, "Content-Type"),
                toString(headers, "Accept")
        )).build();
    }

    private String toString(HttpHeaders headers, String headerName) {
        List<String> vals = headers.getRequestHeader(headerName);
        if (vals.isEmpty()) {
            return "N/A";
        }
        Iterator<String> it = vals.iterator();
        StringBuilder buff = new StringBuilder(it.next());
        while (it.hasNext()) {
            buff.append(",").append(it.next());
        }
        return buff.toString();
    }

    @GET
    @AnonymousAllowed
    @Path("jackfruit")
    public JackFruit get() {
        return new JackFruit("fresh at " + new Date());
    }
}
