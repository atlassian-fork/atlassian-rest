package it.com.atlassian.rest.xsrf;

import com.atlassian.plugins.rest.common.security.jersey.XsrfResourceFilter;
import com.atlassian.plugins.rest.json.JsonObject;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;

import static org.junit.Assert.*;

/**
 */
public class XsrfCheckTest {
    private WebResource.Builder webResource;

    @Before
    public void setUp() {
        webResource = WebResourceFactory.anonymous(WebResourceFactory.REST_VERSION, false)
                .path("xsrfCheck").getRequestBuilder();
    }

    @Test
    public void testGetSuccess() {
        assertSuccessful("GET", webResource);
    }

    @Test
    public void testPostFormBlocked() {
        assertBlocked("POST", webResource.header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    @Test
    public void testPostFormSuccess() {
        assertSuccessful("POST", addXsrf(webResource).header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    @Test
    public void testPostJsonSuccess() {
        assertSuccessful("POST", webResource.entity(new JsonObject()));
    }

    @Test
    public void testPutFormSuccess() {
        assertSuccessful("PUT", webResource.header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    @Test
    public void testPostWithValidXsrfTokenSuccess() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        webResource = WebResourceFactory.anonymous().path("xsrfCheck")
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, xsrfToken).getRequestBuilder();
        webResource = webResource.cookie(xsrfCookie).header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED);
        assertSuccessful("POST", webResource);
    }

    @Test
    public void testPostWithInvalidXsrfTokenBlocked() {
        final String xsrfToken = "abc123";
        Cookie xsrfCookie = new Cookie(IndependentXsrfTokenAccessor.XSRF_COOKIE_KEY, xsrfToken);
        webResource = WebResourceFactory.anonymous(WebResourceFactory.REST_VERSION, false)
                .path("xsrfCheck")
                .queryParam(IndependentXsrfTokenValidator.XSRF_PARAM_NAME, "INCORRECT").getRequestBuilder();
        webResource = webResource.cookie(xsrfCookie).header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED);
        assertBlocked("POST", webResource);
    }

    @Test
    public void testPostFormSuccessWithoutTokenOnXsrfProtectionExcludedAnnotatedResource() {
        webResource = WebResourceFactory.anonymous().path("xsrfCheck")
                .path("xsrfProtectionExcludedResource").getRequestBuilder();
        assertSuccessful("POST", webResource.header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED));
    }

    @Test
    public void testGetWithRequiresXsrfCheckBlocked() {
        webResource = WebResourceFactory.anonymous(WebResourceFactory.REST_VERSION, false)
                .path("xsrfCheck").path(
                        "requiresXsrfCheckAnnotatedResource").getRequestBuilder();
        assertBlocked("GET", webResource);
    }

    private WebResource.Builder addXsrf(WebResource.Builder webResource) {
        return webResource.header(XsrfResourceFilter.TOKEN_HEADER, XsrfResourceFilter.NO_CHECK);
    }

    private void assertSuccessful(String method, WebResource.Builder webResource) {
        assertEquals("Request succeeded", webResource.method(method, String.class));
    }

    private void assertBlocked(String method, WebResource.Builder webResource) {
        try {
            webResource.method(method, String.class);
            fail("Request succeeded");
        } catch (UniformInterfaceException e) {
            assertEquals("XSRF check failed", e.getResponse().getEntity(String.class));
        }
    }
}
