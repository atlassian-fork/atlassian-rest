package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.ExampleResource;
import com.atlassian.plugins.rest.doclet.generators.schema.RichClass;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.ArrayBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SimpleBean;
import com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial.SimpleListBean;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Optional;

import static com.atlassian.plugins.rest.doclet.generators.resourcedoc.RestMethod.restMethod;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class RestMethodTest {
    private final Class<?> resourceClass = ExampleResource.class;
    private final Method createSimple = method("createSimple", String.class, ArrayBean.class);
    private final Method createSimpleWithAutoRequestType = method("createSimpleWithAutoRequestType", String.class, String.class, SimpleBean.class, String.class);
    private final Method methodWithActualReturnType = method("methodWithActualReturnType");
    private final Method methodWithResponseReturnType = method("methodWithResponseReturnType");
    private final Method voidResponseTypeAnnotated = method("voidResponseTypeAnnotated");
    private final Method voidActualReturnType = method("voidActualReturnType");

    @Test
    public void responseTypeFromAnnotationOverridesActualMethodReturnType() {
        assertThat(restMethod(resourceClass, createSimple).responseTypesFor(200), contains(RichClass.of(ArrayBean.class)));
    }

    @Test
    public void requestTypeFromAnnotationOverridesActualParameterFromMethodArgument() {
        assertThat(restMethod(resourceClass, createSimple).getRequestType(), equalTo(Optional.of(RichClass.of(SimpleBean.class))));
    }

    @Test
    public void responseTypeForSuccessCanBeTakenFromReturnType() {
        assertThat(restMethod(resourceClass, methodWithActualReturnType).responseTypesFor(200), contains(RichClass.of(SimpleBean.class)));
    }

    @Test
    public void responseTypeIsNotTakenFromReturnTypeIsResponse() {
        assertThat(restMethod(resourceClass, methodWithResponseReturnType).responseTypesFor(200), hasSize(0));
    }

    @Test
    public void requestTypeCanBeTakenFromParameters() {
        assertThat(restMethod(resourceClass, createSimpleWithAutoRequestType).getRequestType(), equalTo(Optional.of(RichClass.of(SimpleBean.class))));
    }

    @Test
    public void voidResourceDoesNotHaveAnyReturnType() {
        assertThat(restMethod(resourceClass, voidResponseTypeAnnotated).responseTypesFor(200), hasSize(0));
        assertThat(restMethod(resourceClass, voidActualReturnType).responseTypesFor(200), hasSize(0));
    }

    @Test
    public void responseTypeForErrorsCanBeAnnotatedOnClass() {
        assertThat(restMethod(resourceClass, createSimple).responseTypesFor(401), contains(RichClass.of(SimpleListBean.class)));
    }

    private Method method(String methodName, Class<?>... parameterTypes) {
        try {
            return resourceClass.getDeclaredMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
