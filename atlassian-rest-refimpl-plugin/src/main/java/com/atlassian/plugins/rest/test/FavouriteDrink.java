package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.expand.Expander;

import static javax.xml.bind.annotation.XmlAccessType.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(FavouriteDrinkExpander.class)
public class FavouriteDrink {
    @XmlAttribute
    private final String name;

    @XmlElement
    private String description;

    private FavouriteDrink() {
        this(null);
    }

    public FavouriteDrink(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
