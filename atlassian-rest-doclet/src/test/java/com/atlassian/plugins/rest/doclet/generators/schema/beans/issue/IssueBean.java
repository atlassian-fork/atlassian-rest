package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.SelfExpanding;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.net.URI;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @since v4.2
 */
@SuppressWarnings ({ "UnusedDeclaration", "FieldCanBeLocal" })
@XmlRootElement (name = "issue")
public class IssueBean
{
    @XmlAttribute
    private String expand;

    @XmlElement
    private String id;

    @XmlElement
    private URI self;

    @XmlElement
    private String key;

    /*
     * Issue fields. The values in the map will always be a FieldBean.
     */
    @XmlElement
    private Map<String, Object> fields;

    @XmlElement
    private Map<String, Object> renderedFields;

    @XmlTransient
    private final Map<String, Object> renderedFields_ = Maps.newHashMap();

    @XmlTransient
    @Expandable ("renderedFields")
    private SelfExpanding renderedFieldExpander = new SelfExpanding()
    {
        public void expand()
        {
            renderedFields = renderedFields_;
        }
    };

    @XmlElement
    private Map<String, String> names;

    @XmlTransient
    private final Map<String, String> names_ = Maps.newHashMap();

    @XmlTransient
    @Expandable ("names")
    private SelfExpanding namesExpander = new SelfExpanding()
    {
        public void expand()
        {
            names = names_;
        }
    };

    @XmlElement
    private Map<String, JsonTypeBean> schema;

    @XmlTransient
    private final Map<String, JsonTypeBean> schema_ = Maps.newHashMap();

    @XmlTransient
    @Expandable ("schema")
    private SelfExpanding schemaExpander = new SelfExpanding()
    {
        public void expand()
        {
            schema = schema_;
        }
    };

    @XmlElement
    private List<TransitionBean> transitions;

    @XmlTransient
    private final List<TransitionBean> transitions_ = Lists.newArrayList();

    @XmlTransient
    @Expandable ("transitions")
    private SelfExpanding transitionExpander = new SelfExpanding()
    {
        public void expand()
        {
            transitions = transitions_;
        }
    };

    @XmlElement
    private OpsbarBean operations;

    @XmlTransient
    private OpsbarBean operations_;

    @XmlTransient
    @Expandable ("operations")
    private SelfExpanding operationsExpander = new SelfExpanding()
    {
        public void expand()
        {
            operations = operations_;
        }
    };

    @XmlTransient
    @Expandable ("editmeta")
    private SelfExpanding editmetaExpander = new SelfExpanding()
    {
        public void expand()
        {
            editmeta = editmeta_;
        }
    };

    @XmlElement
    private EditMetaBean editmeta;

    @XmlTransient
    private EditMetaBean editmeta_;

    @XmlTransient
    @Expandable ("changelog")
    private SelfExpanding changelogExpander = new SelfExpanding()
    {
        public void expand()
        {
            changelog = changelog_;
        }
    };

    @XmlElement
    private ChangelogBean changelog;
    @XmlTransient
    private ChangelogBean changelog_;

    @XmlTransient
    @Expandable ("versionedRepresentations")
    private SelfExpanding versionedRepresentationsExpander = new SelfExpanding()
    {
        public void expand()
        {
            versionedRepresentations = versionedRepresentations_;
        }
    };

    @XmlElement
    private Map<String, Map<Integer, Object>> versionedRepresentations;

    @XmlTransient
    private final Map<String, Map<Integer, Object>> versionedRepresentations_ = Maps.newHashMap();
}
