package com.atlassian.plugins.rest.test;

import com.google.common.collect.Lists;

import javax.ws.rs.core.UriInfo;
import java.util.List;

class DeveloperStore {
    static List<Developer> getDevelopers(UriInfo uriInfo) {
        return Lists.newArrayList(
                Developer.getDeveloper("developer1", uriInfo),
                Developer.getDeveloper("developer2", uriInfo),
                Developer.getDeveloper("developer3", uriInfo),
                Developer.getDeveloper("developer4", uriInfo),
                Developer.getDeveloper("developer5", uriInfo),
                Developer.getDeveloper("developer6", uriInfo));
    }
}
