package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;

@JsonAutoDetect
public class SimpleBean
{
    @JsonIgnore
    public boolean a;

    void getVoid()
    {

    }

    public void setA(boolean a) {} // test properties without getters but with setters
}
