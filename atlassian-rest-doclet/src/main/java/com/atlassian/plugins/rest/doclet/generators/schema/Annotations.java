package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.rest.annotation.RestProperty;
import com.atlassian.rest.annotation.RestProperty.Scope;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.google.common.collect.ImmutableSet;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.plugins.rest.doclet.generators.schema.Types.isJDKClass;
import static com.google.common.base.Strings.emptyToNull;

public final class Annotations {
    private Annotations() {
    }

    private static final Set<Class<? extends Annotation>> propertyAnnotations = ImmutableSet.of(
            JsonProperty.class,
            com.fasterxml.jackson.annotation.JsonProperty.class,
            XmlAttribute.class,
            XmlElement.class,
            XmlJavaTypeAdapter.class
    );

    private static final Set<Class<? extends Annotation>> autodetectAnnotations = ImmutableSet.of(
            JsonAutoDetect.class,
            com.fasterxml.jackson.annotation.JsonAutoDetect.class,
            XmlRootElement.class
    );

    private static final Set<Class<? extends Annotation>> ignoreAnnotations = ImmutableSet.of(
            JsonIgnore.class,
            com.fasterxml.jackson.annotation.JsonIgnore.class,
            XmlTransient.class
    );

    public static boolean shouldFieldBeIncludedInSchema(final AnnotatedElement element, String name, final Class<?> modelClass, final Scope scope) {
        boolean isJsonField = isCustomInterface(modelClass) || isAnyAnnotationPresent(element, propertyAnnotations) ||
                element instanceof Field && isAnyAnnotationPresent(modelClass, autodetectAnnotations);

        Scope fieldScope = element.isAnnotationPresent(RestProperty.class) ? element.getAnnotation(RestProperty.class).scope() : Scope.AUTO;

        return isJsonField && !isIgnored(name, modelClass) && scope.includes(fieldScope, name);
    }

    private static boolean isIgnored(final String name, final Class<?> modelClass) {
        try {
            for (PropertyDescriptor propertyDescriptor : getPropertyDescriptors(modelClass)) {
                Method getter = propertyDescriptor.getReadMethod();
                if (getter != null && propertyDescriptor.getName().equals(name) && isAnyAnnotationPresent(getter, ignoreAnnotations)) {
                    return true;
                }
            }

            Field field = modelClass.getDeclaredField(name);
            return field != null && isAnyAnnotationPresent(field, ignoreAnnotations);
        } catch (NoSuchFieldException ex) {
            return false;
        }
    }

    private static boolean isCustomInterface(final Class<?> modelClass) {
        return !isJDKClass(modelClass) && modelClass.isInterface();
    }

    public static String resolveFieldName(final AnnotatedElement element, String defaultName) {
        if (element.isAnnotationPresent(JsonProperty.class)) {
            return Optional.ofNullable(emptyToNull(element.getAnnotation(JsonProperty.class).value())).orElse(defaultName);
        } else if (element.isAnnotationPresent(XmlElement.class)) {
            String name = element.getAnnotation(XmlElement.class).name();
            return !name.equals("##default") ? name : defaultName;
        } else if (element.isAnnotationPresent(XmlAttribute.class)) {
            String name = element.getAnnotation(XmlAttribute.class).name();
            return !name.equals("##default") ? name : defaultName;
        } else {
            return defaultName;
        }
    }

    public static boolean isRequired(final AnnotatedElement element) {
        return element.isAnnotationPresent(com.fasterxml.jackson.annotation.JsonProperty.class) &&
                element.getAnnotation(com.fasterxml.jackson.annotation.JsonProperty.class).required() ||
                element.isAnnotationPresent(RestProperty.class) && element.getAnnotation(RestProperty.class).required();
    }

    public static String getDescription(final AnnotatedElement field) {
        if (field != null) {
            if (field.isAnnotationPresent(JsonPropertyDescription.class)) {
                return emptyToNull(field.getAnnotation(JsonPropertyDescription.class).value());
            } else if (field.isAnnotationPresent(RestProperty.class)) {
                return emptyToNull(field.getAnnotation(RestProperty.class).description());
            }
        }
        return null;
    }

    private static boolean isAnyAnnotationPresent(AnnotatedElement element, Iterable<Class<? extends Annotation>> annotations) {
        for (Class<? extends Annotation> annotation : annotations) {
            if (element.isAnnotationPresent(annotation)) {
                return true;
            }
        }
        return false;
    }

    private static Iterable<PropertyDescriptor> getPropertyDescriptors(Class<?> type) {
        try {
            return Arrays.asList(Introspector.getBeanInfo(type).getPropertyDescriptors());
        } catch (IntrospectionException e) {
            return Collections.emptyList();
        }
    }
}
