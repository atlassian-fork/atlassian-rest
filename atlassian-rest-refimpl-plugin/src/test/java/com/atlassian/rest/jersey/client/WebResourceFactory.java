package com.atlassian.rest.jersey.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;

/**
 * Gets {@link WebResource web resources} for testing.
 */
public final class WebResourceFactory {
    public static final String LATEST = "latest";
    public static final String REST_VERSION = System.getProperty("refimpl.rest.version", "1");
    public static final String REST_VERSION_2 = System.getProperty("refimpl2.rest.version", "2");

    private static final String ADMIN_USERNAME = "admin";
    private static final String ADMIN_PASSWORD = "admin";
    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    static {
        System.out.println("Base URI (latest) is <" + getRefImplWebResourceUri(LATEST) + ">");
        System.out.println("Base URI (version 1) is <" + getRefImplWebResourceUri(REST_VERSION) + ">");
        System.out.println("Base URI (version 2) is <" + getRefImplWebResourceUri(REST_VERSION_2) + ">");
    }

    public static WebResource authenticated(String version, boolean addXsrfHeader) {
        return authenticate(ADMIN_USERNAME, ADMIN_PASSWORD, version, addXsrfHeader);
    }

    public static WebResource authenticated(String version) {
        return authenticate(ADMIN_USERNAME, ADMIN_PASSWORD, version, true);
    }

    public static WebResource authenticated() {
        return authenticated(REST_VERSION);
    }

    public static WebResource anonymous(String version) {
        return anonymous(version, true);
    }

    public static WebResource anonymous(URI uri) {
        return createClient(true).resource(uri);
    }

    public static WebResource anonymous(String version, boolean addXsrfHeader) {
        return createClient(addXsrfHeader).resource(getRefImplWebResourceUri(version));
    }

    public static WebResource anonymous() {
        return anonymous(REST_VERSION);
    }

    public static WebResource authenticate(String username, String password) {
        return authenticate(username, password, REST_VERSION, true);
    }

    public static WebResource authenticate(String username, String password, String version) {
        return authenticate(username, password, version, true);
    }

    public static WebResource authenticate(String username, String password, String version, boolean addXsrfHeader) {
        Client client = createClient(addXsrfHeader);
        client.addFilter(new HTTPBasicAuthFilter(username, password));
        return client.resource(getRefImplWebResourceUri(version));
    }

    public static UriBuilder getUriBuilder() {
        return UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT);
    }

    private static URI getRefImplWebResourceUri(String version) {
        return getUriBuilder().path("rest").path("refimpl").path(version).build();
    }

    private static Client createClient(boolean addXsrfHeader) {
        Client client = Client.create();
        if (addXsrfHeader) {
            client.addFilter(new XsrfHeaderClientFilter());
        }
        return client;
    }
}
