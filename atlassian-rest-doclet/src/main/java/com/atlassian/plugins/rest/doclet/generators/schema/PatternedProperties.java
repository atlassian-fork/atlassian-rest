package com.atlassian.plugins.rest.doclet.generators.schema;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import java.util.Objects;

public class PatternedProperties {

    private final String pattern;
    private final ModelClass valuesType;

    public PatternedProperties(String pattern, ModelClass valuesType) {
        this.pattern = Preconditions.checkNotNull(pattern);
        this.valuesType = Preconditions.checkNotNull(valuesType);
    }

    public String getPattern() {
        return pattern;
    }

    public ModelClass getValuesType() {
        return valuesType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PatternedProperties that = (PatternedProperties) o;

        return Objects.equals(this.pattern, that.pattern) &&
                Objects.equals(this.valuesType, that.valuesType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pattern, valuesType);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("pattern", pattern)
                .add("valuesType", valuesType)
                .toString();
    }
}
