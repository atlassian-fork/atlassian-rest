package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class TestAbstractBean
{
    private AbstractValue value;
}

@JsonAutoDetect
abstract class AbstractValue {
    String base;
}

@JsonAutoDetect
class A extends AbstractValue {
    Integer a;
}

@JsonAutoDetect
class B extends AbstractValue {
    boolean b;
}