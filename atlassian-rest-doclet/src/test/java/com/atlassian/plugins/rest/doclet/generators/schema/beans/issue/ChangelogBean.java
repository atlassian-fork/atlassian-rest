package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @since v5.0
 */
public class ChangelogBean
{
    @JsonProperty
    private Integer startAt;

    @JsonProperty
    private Integer maxResults;

    @JsonProperty
    private Integer total;

    @JsonProperty
    private List<ChangeHistoryBean> histories;

    public static class ChangeHistoryBean
    {
        @JsonProperty
        private String id;

        @JsonProperty
        private UserJsonBean author;

        @JsonProperty
        @XmlJavaTypeAdapter (XmlAdapter.class)
        private Date created;

        @JsonProperty
        private List<ChangeItemBean> items;

        /**
         * @since JIRA 6.3
         */
        @JsonProperty
        @JsonSerialize (include = JsonSerialize.Inclusion.NON_NULL)
        @XmlElement (nillable = true)
        private HistoryMetadata historyMetadata;
    }

    public static class ChangeItemBean
    {
        @JsonProperty
        private String field;
        @JsonProperty
        private String fieldtype;

        @JsonProperty
        @JsonSerialize (include = JsonSerialize.Inclusion.ALWAYS)
        @XmlElement (nillable = true)
        private String from;
        @JsonProperty
        @JsonSerialize (include = JsonSerialize.Inclusion.ALWAYS)
        @XmlElement (nillable = true)
        private String fromString;
        @JsonProperty
        @JsonSerialize (include = JsonSerialize.Inclusion.ALWAYS)
        @XmlElement (nillable = true)
        private String to;
        @JsonProperty
        @JsonSerialize (include = JsonSerialize.Inclusion.ALWAYS)
        @XmlElement (nillable = true)
        private String toString;
    }
}
