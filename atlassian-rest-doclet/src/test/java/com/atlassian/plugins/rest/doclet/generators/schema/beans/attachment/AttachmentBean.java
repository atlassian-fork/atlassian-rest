package com.atlassian.plugins.rest.doclet.generators.schema.beans.attachment;

import org.joda.time.Instant;
import org.joda.time.ReadableInstant;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * @since v4.2
 */
@XmlRootElement (name = "attachment")
public class AttachmentBean
{
    @XmlElement
    private URI self;

    @XmlElement
    private String filename;

    @XmlElement
    private UserBean author;

    @XmlJavaTypeAdapter (XmlAdapter.class)
    private Date created;

    @XmlElement
    private long size;

    @XmlElement
    private String mimeType;

    // HACK (LGM) changed to a HashMap from PropertySet in order to get doc auto-generation working
    @XmlElement
    private HashMap<String, Object> properties;

    @XmlElement
    private String content;

    @XmlElement
    private String thumbnail;
}
