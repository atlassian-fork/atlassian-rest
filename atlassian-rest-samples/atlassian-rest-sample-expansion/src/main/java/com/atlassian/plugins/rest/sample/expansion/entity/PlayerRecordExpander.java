package com.atlassian.plugins.rest.sample.expansion.entity;

import com.atlassian.plugins.rest.common.expand.AbstractRecursiveEntityExpander;
import com.atlassian.plugins.rest.sample.expansion.resource.DataStore;

/**
 * Expands a {@link PlayerRecord} by asking the {@link DataStore} to perform the database queries.
 */
public class PlayerRecordExpander extends AbstractRecursiveEntityExpander<PlayerRecord> {
    protected PlayerRecord expandInternal(PlayerRecord entity) {
        if (entity != null && entity.getPlayer() != null)
            return DataStore.getInstance().getPlayerRecord(entity.getPlayer());
        else
            return entity;
    }
}
