package it.com.atlassian.rest.json;

import com.atlassian.rest.jersey.client.WebResourceFactory;

import static org.junit.Assert.*;

import org.junit.Test;

public class JsonTest {
    @Test
    public void testConfiguration() {
        final String jsonString = WebResourceFactory.anonymous().path("json.json").get(String.class);
        assertEquals("{\"anInteger\":1,\"aLong\":2,\"emptyCollection\":[],\"singletonCollection\":[{\"name\":\"item1\"}],\"singletonMap\":{\"foo\":\"bar\"}}", jsonString);
    }

    @Test
    public void testReturningChildClassMeansSerializingAsChildClass() {
        final String jsonString = WebResourceFactory.anonymous().path("dummyjson/subclass.json").get(String.class);
        assertEquals("{\"dataFromParent\":\"parent\",\"dataFromChild\":\"child\"}", jsonString);
    }

    @Test
    public void testReturningGenericClassShouldNotThrowException() {
        final String jsonString = WebResourceFactory.anonymous().path("dummyjson/generic.json").get(String.class);
        assertEquals("{\"dataFromParent\":\"parent\"}", jsonString);
    }

    @Test
    public void testReturningGenericChildClassMeansSerializingAsChildClass() {
        final String jsonString = WebResourceFactory.anonymous().path("dummyjson/subclassgeneric.json").get(String.class);
        assertEquals("{\"dataFromParent\":\"parent\",\"dataFromChild\":\"child\"}", jsonString);
    }

    @Test
    public void testSubClassGenericExplicitlyMeansSerializingAsChildClass() {
        final String jsonString = WebResourceFactory.anonymous().path("dummyjson/subclassgenericexplicit.json").get(String.class);
        assertEquals("{\"dataFromParent\":\"parent\",\"dataFromChild\":\"child\"}", jsonString);
    }
}
