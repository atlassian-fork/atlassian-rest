package com.atlassian.plugins.rest.doclet.generators.schema;

import com.atlassian.rest.annotation.RestProperty;
import com.atlassian.rest.annotation.RestProperty.Scope;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import org.reflections.Reflections;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.plugins.rest.doclet.generators.schema.Annotations.shouldFieldBeIncludedInSchema;
import static com.atlassian.plugins.rest.doclet.generators.schema.Types.isCollection;
import static com.atlassian.plugins.rest.doclet.generators.schema.Types.isJDKClass;
import static com.atlassian.plugins.rest.doclet.generators.schema.Types.isPrimitive;
import static com.atlassian.plugins.rest.doclet.generators.schema.Types.resolveType;
import static java.lang.reflect.Modifier.isStatic;
import static java.util.stream.Collectors.toList;

public final class ModelClass implements Comparable<ModelClass> {

    private final Class<?> actualClass;
    private final RichClass richClass;
    private final AnnotatedElement containingField;
    private final Schema.Type schemaType;

    public ModelClass(RichClass richClass, AnnotatedElement containingField) {
        this.actualClass = richClass.getActualClass();
        this.richClass = richClass;
        this.schemaType = resolveType(richClass, containingField);
        this.containingField = containingField;
    }

    public Class<?> getActualClass() {
        return actualClass;
    }

    public Schema.Type getType() {
        return schemaType;
    }

    public String getDescription() {
        return Annotations.getDescription(containingField);
    }

    public String getTopLevelTitle() {
        if (richClass.hasGenericType()) {
            String wrappedTitle = Joiner.on("-and-").skipNulls().join(Iterables.transform(richClass.getGenericTypes(), new Function<RichClass, String>() {
                @Override
                public String apply(final RichClass input) {
                    return new ModelClass(input, null).getTitle();
                }
            }));

            return Strings.emptyToNull(wrappedTitle) != null ? (getTitle(actualClass) + " of " + wrappedTitle) : null;
        } else {
            return getTitle(actualClass);
        }
    }

    public String getTitle() {
        if (isPrimitive(actualClass) || isCollection(richClass) || isJDKClass(actualClass)) {
            return null;
        } else {
            return getTitle(actualClass);
        }
    }

    private String getTitle(Class<?> actualClass) {
        String simplifiedClassName = actualClass.getSimpleName().replaceAll("(Bean|Data|DTO|Dto|Json|JSON|Enum)+$", "").replaceAll("^Abstract", "");
        return camelCaseToSpaces(simplifiedClassName);
    }

    private String camelCaseToSpaces(String camelCaseName) {
        String[] parts = camelCaseName.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
        return Joiner.on(" ").join(parts);
    }

    public Set<Property> getProperties(Scope scope) {
        return schemaType == Schema.Type.Object ? Sets.newLinkedHashSet(getProperties(actualClass, scope)) : Collections.<Property>emptySet();
    }

    public List<Property> getProperties(Class<?> actualClass, Scope scope) {
        List<Property> result = Lists.newArrayList();

        if (schemaType == Schema.Type.Object && actualClass != Object.class && actualClass != null) {
            for (Field field : actualClass.getDeclaredFields()) {
                if (!isStatic(field.getModifiers()) && shouldFieldBeIncludedInSchema(field, field.getName(), actualClass, scope)) {
                    ModelClass propertyModel = new ModelClass(richClass.createContainedType(field.getGenericType()), field);
                    result.add(new Property(propertyModel, Annotations.resolveFieldName(field, field.getName()), Annotations.isRequired(field)));
                }
            }

            for (PropertyDescriptor descriptor : getPropertyDescriptors(actualClass)) {
                Method getter = descriptor.getReadMethod();
                if (getter != null && getter.getDeclaringClass() != Object.class && shouldFieldBeIncludedInSchema(getter, descriptor.getName(), actualClass, scope)) {
                    ModelClass propertyModel = new ModelClass(richClass.createContainedType(getter.getGenericReturnType()), getter);
                    if (!result.contains(propertyModel)) {
                        result.add(new Property(propertyModel, Annotations.resolveFieldName(getter, descriptor.getName()), Annotations.isRequired(getter)));
                    }
                }
            }

            result.addAll(0, getProperties(actualClass.getSuperclass(), scope));
        }

        return result;
    }

    public Optional<PatternedProperties> getPatternedProperties() {
        if (Map.class.isAssignableFrom(actualClass) && richClass.getGenericTypes().size() == 2) {
            String pattern = containingField != null && containingField.isAnnotationPresent(RestProperty.class) ?
                    containingField.getAnnotation(RestProperty.class).pattern() : ".+";

            return Optional.of(new PatternedProperties(pattern, new ModelClass(richClass.getGenericTypes().get(1), null)));
        }
        return Optional.empty();
    }

    public Optional<ModelClass> getCollectionItemModel() {
        if (getType() == Schema.Type.Array) {
            return Optional.of(new ModelClass(richClass.getGenericTypes().get(0), null));
        }
        return Optional.empty();
    }

    public boolean isAbstract() {
        return Modifier.isAbstract(actualClass.getModifiers()) && !actualClass.isInterface() && !isJDKClass(actualClass) && actualClass.getPackage() != null;
    }

    public List<ModelClass> getSubModels() {
        Set<ModelClass> result = Sets.newTreeSet();

        if (isAbstract() && actualClass.getTypeParameters().length == 0) // generic abstract classes are not supported, because they are probably some kind of collections
        {
            Reflections reflections = new Reflections(actualClass.getPackage().getName()); // search only in the package class

            for (Class<?> aClass : reflections.getSubTypesOf(actualClass)) // get subclasses of this
            {
                result.add(new ModelClass(RichClass.of(aClass), null));
            }
        }

        return ImmutableList.copyOf(result);
    }

    public Set<ModelClass> getSchemasReferencedMoreThanOnce(final Scope scope) {
        Multiset<ModelClass> referenceCount = HashMultiset.create();
        computeSchemasReferencedMoreThanOnce(this, scope, referenceCount);

        ImmutableSet.Builder<ModelClass> result = ImmutableSet.builder();
        for (ModelClass modelClass : referenceCount) {
            if (modelClass.getTitle() != null && referenceCount.count(modelClass) > 1) {
                result.add(modelClass);
            }
        }

        return result.build();
    }

    private static void computeSchemasReferencedMoreThanOnce(ModelClass currentNode, final Scope scope, Multiset<ModelClass> alreadyReferenced) {
        List<ModelClass> propertyModels = currentNode.getProperties(scope).stream()
                .map(input -> input.model)
                .collect(toList());


        List<ModelClass> subModels = currentNode.getSubModels();
        for (ModelClass subClass : subModels) {
            alreadyReferenced.add(subClass, 2); // let's write definitions for all sub models as it will be more readable in "oneOf" clauses
        }

        List<ModelClass> firstLevelModels = Lists.newArrayList();
        firstLevelModels.addAll(subModels);
        firstLevelModels.addAll(propertyModels);
        currentNode.getPatternedProperties().map(PatternedProperties::getValuesType).ifPresent(firstLevelModels::add);
        currentNode.getCollectionItemModel().ifPresent(firstLevelModels::add);

        for (ModelClass firstLevelModel : firstLevelModels) {
            alreadyReferenced.add(firstLevelModel);
            if (alreadyReferenced.count(firstLevelModel) <= 1 || Types.isCollection(firstLevelModel.richClass) || Map.class.isAssignableFrom(firstLevelModel.actualClass)) {
                computeSchemasReferencedMoreThanOnce(firstLevelModel, scope, alreadyReferenced);
            }
        }
    }

    private PropertyDescriptor[] getPropertyDescriptors(final Class<?> actualClass) {
        try {
            return Introspector.getBeanInfo(actualClass).getPropertyDescriptors();
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelClass that = (ModelClass) o;

        return Objects.equals(this.actualClass, that.actualClass);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(actualClass);
    }

    @Override
    public int compareTo(final ModelClass o) {
        return actualClass.getName().compareTo(o.getActualClass().getName());
    }
}
