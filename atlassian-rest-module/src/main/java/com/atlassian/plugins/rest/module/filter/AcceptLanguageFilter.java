package com.atlassian.plugins.rest.module.filter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.core.header.InBoundHeaders;
import com.sun.jersey.core.header.LanguageTag;
import com.sun.jersey.core.header.QualityFactor;
import com.sun.jersey.core.header.reader.HttpHeaderReader;
import com.sun.jersey.spi.container.AdaptingContainerRequest;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

/**
 * Filter wraps ContainerRequest to change the way how AcceptableLanguage header parsed
 *
 * By default Jersey doesn't process well UN M.49 region codes. Filter fixes that by wrapping
 * ContainerRequest and replace method that parses AcceptableLanguage header
 *
 * @author pandronov
 */
@Provider
public class AcceptLanguageFilter implements ContainerRequestFilter {
    // HttpHeaderReader#QUALITY_COMPARATOR direct copy (private scope... :()
    private static final Comparator<QualityFactor> QUALITY_COMPARATOR = new Comparator<QualityFactor>() {
        public int compare(QualityFactor o1, QualityFactor o2) {
            return o2.getQuality() - o1.getQuality();
        }
    };

    private static final CustomLanguageTag ANY_LANG = new CustomLanguageTag("*", null);

    @Override
    public ContainerRequest filter(ContainerRequest request) {
        return new AdaptingContainerRequest(request) {

            private List<Locale> acceptLanguages;

            @Override
            public void setHeaders(InBoundHeaders headers) {
                // As usual jersey hides everything in private scope, so
                // nice headersModCount is not available :(
                super.setHeaders(headers);
                acceptLanguages = null;
            }

            @Override
            public List<Locale> getAcceptableLanguages() {
                if (acceptLanguages == null) {
                    List<CustomLanguageTag> alts = parseAcceptLanguage();

                    acceptLanguages = new ArrayList<Locale>(alts.size());
                    for (CustomLanguageTag alt : alts) {
                        acceptLanguages.add(alt.getAsLocale());
                    }
                }

                return acceptLanguages;
            }

            // Copy of HttpHelper & HttpHeaderReader code adjusted to accept
            // UN M.49 region codes
            private List<CustomLanguageTag> parseAcceptLanguage() {
                final String acceptLanguage = getHeaderValue(HttpHeaders.ACCEPT_LANGUAGE);
                if (acceptLanguage == null || acceptLanguage.length() == 0) {
                    return Collections.singletonList(ANY_LANG);
                }

                try {
                    List<CustomLanguageTag> result = new ArrayList<>();
                    HttpHeaderReader reader = HttpHeaderReader.newInstance(acceptLanguage);
                    HttpHeaderListAdapter adapter = new HttpHeaderListAdapter(reader);
                    while (reader.hasNext()) {
                        result.add(parserLanguageTag(adapter));
                        adapter.reset();

                        if (reader.hasNext()) {
                            reader.next();
                        }
                    }

                    Collections.sort(result, QUALITY_COMPARATOR);
                    return result;
                } catch (java.text.ParseException e) {
                    throw new WebApplicationException(
                            e,
                            Response.status(Response.Status.BAD_REQUEST)
                                    .entity("Bad Accept-Language header value: '" + acceptLanguage + "'")
                                    .type("text/plain")
                                    .build()
                    );
                }
            }

            // Parse core from LanguageTag 
            private CustomLanguageTag parserLanguageTag(HttpHeaderReader reader) throws ParseException {
                // Skip any white space
                reader.hasNext();
                String primaryTag = null, subTags = null, languageTag = reader.nextToken();

                if (!languageTag.equals("*")) {
                    if (!isLanguageTagValid(languageTag)) {
                        throw new ParseException("String, " + languageTag + ", is not a valid language tag", 0);
                    }

                    final int index = languageTag.indexOf('-');
                    if (index == -1) {
                        primaryTag = languageTag;
                        subTags = null;
                    } else {
                        primaryTag = languageTag.substring(0, index);
                        subTags = languageTag.substring(index + 1, languageTag.length());
                    }
                } else {
                    primaryTag = languageTag;
                }

                int quality;
                if (reader.hasNext()) {
                    quality = HttpHeaderReader.readQualityFactorParameter(reader);
                } else {
                    quality = CustomLanguageTag.DEFAULT_QUALITY_FACTOR;
                }

                return new CustomLanguageTag(languageTag, primaryTag, subTags, quality);
            }

            // Parse core from LanguageTag, modified to accept UN M.49 codes 
            private boolean isLanguageTagValid(String tag) {
                int alphaCount = 0, parts = 0;
                for (int i = 0; i < tag.length(); i++) {
                    final char c = tag.charAt(i);
                    if (c == '-') {
                        if (alphaCount == 0) {
                            return false;
                        }
                        alphaCount = 0;
                        parts++;
                    } else if (
                            ('A' <= c && c <= 'Z') ||
                                    ('a' <= c && c <= 'z') ||
                                    (Character.isDigit(c) && (parts > 0 || alphaCount > 0))
                            ) {
                        alphaCount++;
                        if (alphaCount > 8)
                            return false;
                    } else {
                        return false;
                    }
                }
                return (alphaCount != 0);
            }
        };
    }

    // Copy of LanguageTag & AcceptableLanguageTag - private scope & final methods.... :(
    private static final class CustomLanguageTag implements QualityFactor {

        protected int quality = DEFAULT_QUALITY_FACTOR;

        protected String tag;

        protected String primaryTag;

        protected String subTags;

        public CustomLanguageTag(String primaryTag, String subTags) {
            this(
                    (subTags != null && subTags.length() > 0 ? primaryTag + "-" + subTags : primaryTag),
                    primaryTag,
                    subTags,
                    DEFAULT_QUALITY_FACTOR
            );
        }

        public CustomLanguageTag(String tag, String primaryTag, String subTags, int quality) {
            this.tag = tag;
            this.primaryTag = primaryTag;
            this.subTags = subTags;
            this.quality = quality;
        }

        public final Locale getAsLocale() {
            return (subTags == null)
                    ? new Locale(primaryTag)
                    : new Locale(primaryTag, subTags);
        }

        @Override
        public int getQuality() {
            return quality;
        }

        @Override
        public boolean equals(Object object) {
            if (object instanceof LanguageTag) {
                LanguageTag lt = (LanguageTag) object;

                if (this.tag != null)
                    if (!this.tag.equals(lt.getTag()))
                        return false;
                    else if (lt.getTag() != null)
                        return false;

                if (this.primaryTag != null)
                    if (!this.primaryTag.equals(lt.getPrimaryTag()))
                        return false;
                    else if (lt.getPrimaryTag() != null)
                        return false;

                if (this.subTags != null)
                    if (!this.subTags.equals(lt.getSubTags()))
                        return false;
                    else if (lt.getSubTags() != null)
                        return false;

                return true;
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return (tag == null ? 0 : tag.hashCode()) +
                    (primaryTag == null ? 0 : primaryTag.hashCode()) +
                    (subTags == null ? 0 : primaryTag.hashCode());
        }

        @Override
        public String toString() {
            return primaryTag + (subTags == null ? "" : subTags);
        }
    }
}
