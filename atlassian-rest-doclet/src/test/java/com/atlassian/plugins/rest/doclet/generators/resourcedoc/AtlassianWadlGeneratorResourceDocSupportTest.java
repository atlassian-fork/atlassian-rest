package com.atlassian.plugins.rest.doclet.generators.resourcedoc;

import com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.unversioned.UnversionedResource;
import com.atlassian.plugins.rest.doclet.generators.resourcedoc.resource.versioned.VersionedResource;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.api.model.AbstractResourceMethod;
import com.sun.jersey.server.wadl.WadlGenerator;
import com.sun.jersey.server.wadl.WadlGeneratorImpl;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import com.sun.research.ws.wadl.Resource;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;

import static org.junit.Assert.assertEquals;

public class AtlassianWadlGeneratorResourceDocSupportTest {
    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void testGeneratedDocUrlDoesNotContainNoneVersion() throws Exception {
        System.setProperty("atlassian-plugin.xml.path", "target/test-classes/plugin");
        assertGeneratedDocUrl("blah/latest");
    }

    @Test
    public void testAtlassianPluginXmlShouldBeLoadedFromClasspathIfPathPropertyNotSet() throws Exception {
        assertGeneratedDocUrl("vr");
    }

    @Test
    public void testAtlassianPluginXmlShouldBeLoadedFromJarClasspathIfPathPropertyNotSet() throws Exception {
        AtlassianWadlGeneratorResourceDocSupport generator = new MockAtlassianWadlGeneratorResourceDocSupport(new WadlGeneratorImpl(), new ResourceDocType());
        assertGeneratedDocUrl(generator, "fromjar");
    }

    @Test
    public void testGeneratedDocUrlShouldNotDuplicatePath() throws Exception {
        AtlassianWadlGeneratorResourceDocSupport generator = new AtlassianWadlGeneratorResourceDocSupport(new WadlGeneratorImpl(), new ResourceDocType());
        generator.setResourceDocStream(generator.getClass().getClassLoader().getResourceAsStream("versionedsampleresourcedoc.xml"));
        generator.init();

        AbstractResource r = new AbstractResource(VersionedResource.class);
        r.getResourceMethods().add(new AbstractResourceMethod(r, VersionedResource.class.getMethods()[0], Void.class, Void.class, "POST", new Annotation[]{}));
        Resource documentedResource = generator.createResource(r, "api/2/doSomething");
        assertEquals("api/2/doSomething", documentedResource.getPath());
    }

    private void assertGeneratedDocUrl(String path) throws Exception {
        AtlassianWadlGeneratorResourceDocSupport generator = new AtlassianWadlGeneratorResourceDocSupport(new WadlGeneratorImpl(), new ResourceDocType());
        assertGeneratedDocUrl(generator, path);
    }

    private void assertGeneratedDocUrl(AtlassianWadlGeneratorResourceDocSupport generator, String path) throws Exception {
        generator.setResourceDocStream(generator.getClass().getClassLoader().getResourceAsStream("sampleresourcedoc.xml"));
        generator.init();

        AbstractResource r = new AbstractResource(UnversionedResource.class);
        r.getResourceMethods().add(new AbstractResourceMethod(r, UnversionedResource.class.getMethods()[0], Void.class, Void.class, "POST", new Annotation[]{}));
        Resource documentedResource = generator.createResource(r, "doSomething");
        assertEquals(path + "/doSomething", documentedResource.getPath());
    }

    private class MockAtlassianWadlGeneratorResourceDocSupport extends AtlassianWadlGeneratorResourceDocSupport {

        MockAtlassianWadlGeneratorResourceDocSupport(WadlGenerator wadlGenerator, ResourceDocType resourceDoc) {
            super(wadlGenerator, resourceDoc);
        }

        protected Enumeration<URL> loadClasspathPluginXML() throws IOException {
            //the sample-plugin.jar contains an atlassian-plugin.xml with below rest definition
            /*
                 <rest key="versionless-resource" path="/fromjar" version="none">
                    <package>com.atlassian.plugins.rest.doclet.generators.resourcedoc</package>
                </rest>z
            */
            String pluginPath = this.getClass().getClassLoader().getResource("sample-plugin.jar").getPath();
            Vector<URL> vector = new Vector<>();
            vector.add(new URL("jar:file:" + pluginPath + "!/atlassian-plugin.xml"));
            return vector.elements();
        }
    }

}
