package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.joda.time.DateTime;

import java.util.Date;

@JsonAutoDetect
public class DateTimeBean
{
    private Date date;
    private DateTime dateTime;

    public Date getDate()
    {
        return date;
    }

    public void setDate(final Date date)
    {
        this.date = date;
    }

    public DateTime getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(final DateTime dateTime)
    {
        this.dateTime = dateTime;
    }
}
