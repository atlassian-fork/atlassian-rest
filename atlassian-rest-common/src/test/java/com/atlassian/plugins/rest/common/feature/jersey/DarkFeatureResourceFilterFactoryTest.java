package com.atlassian.plugins.rest.common.feature.jersey;

import org.junit.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class DarkFeatureResourceFilterFactoryTest extends AbstractDarkFeatureResourceFilterTest {
    @InjectMocks
    private DarkFeatureResourceFilterFactory classUnderTest;

    @Test
    public void testCreateWithNoAnnotations() {
        setResourceAnnotated(false);
        setMethodAnnotated(false);

        assertThat(classUnderTest.create(abstractMethod), empty());
    }

    @Test
    public void testCreateWithResourceAnnotations() {
        setResourceAnnotated(true);
        setMethodAnnotated(false);

        assertThat(classUnderTest.create(abstractMethod), hasSize(1));
    }

    @Test
    public void testCreateWithMethodAnnotations() {
        setResourceAnnotated(false);
        setMethodAnnotated(true);

        assertThat(classUnderTest.create(abstractMethod), hasSize(1));
    }

    @Test
    public void testCreateWithResourceAndMethodAnnotations() {
        setResourceAnnotated(true);
        setMethodAnnotated(true);

        assertThat(classUnderTest.create(abstractMethod), hasSize(1));
    }
}