package com.atlassian.plugins.rest.common.multipart.fileupload;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.FileSizeLimitExceededException;
import com.atlassian.plugins.rest.common.multipart.MultipartForm;
import com.atlassian.plugins.rest.common.multipart.MultipartHandler;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;

public class CommonsFileUploadMultipartHandler implements MultipartHandler {
    private final ServletFileUpload servletFileUpload;

    public CommonsFileUploadMultipartHandler(long maxFileSize, long maxSize) {
        servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
        servletFileUpload.setFileSizeMax(maxFileSize);
        servletFileUpload.setSizeMax(maxSize);
    }

    public FilePart getFilePart(HttpServletRequest request, String field) {
        return getForm(request).getFilePart(field);
    }

    private static class CommonsFileUploadMultipartForm implements
            MultipartForm {
        private final Collection<FileItem> fileItems;

        private CommonsFileUploadMultipartForm(
                final Collection<FileItem> fileItems) {
            this.fileItems = fileItems;
        }

        public FilePart getFilePart(String field) {
            for (FileItem item : fileItems) {
                if (item.getFieldName().equals(field)) {
                    return new CommonsFileUploadFilePart(item);
                }
            }
            return null;
        }

        public Collection<FilePart> getFileParts(String field) {
            Collection<FilePart> fileParts = new ArrayList<FilePart>();
            for (FileItem item : fileItems) {
                if (item.getFieldName().equals(field)) {
                    fileParts.add(new CommonsFileUploadFilePart(item));

                }
            }
            return fileParts;
        }
    }

    public MultipartForm getForm(HttpServletRequest request) {
        return getForm(new ServletRequestContext(request));
    }

    public MultipartForm getForm(RequestContext request) {
        try {
            return new CommonsFileUploadMultipartForm(
                    servletFileUpload.parseRequest(request));
        } catch (FileUploadException e) {
            if (e instanceof FileUploadBase.FileSizeLimitExceededException
                    || e instanceof FileUploadBase.SizeLimitExceededException) {
                throw new FileSizeLimitExceededException(e.getMessage());
            }
            throw new RuntimeException(e);
        }
    }
}
