package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import com.atlassian.plugins.rest.common.security.AuthorisationException;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

import javax.ws.rs.ext.Provider;

import static java.util.Objects.requireNonNull;

/**
 * Filter that can be used to restrict access to resources to administrators.
 *
 * @since 2.7.1
 */
@Provider
public class AdminOnlyResourceFilter implements ResourceFilter, ContainerRequestFilter {
    private final UserManager userManager;

    public AdminOnlyResourceFilter(UserManager userManager) {
        this.userManager = requireNonNull(userManager);
    }

    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    public ContainerRequest filter(final ContainerRequest containerRequest) {
        UserKey userKey = userManager.getRemoteUserKey();
        if (userKey == null) {
            throw new AuthenticationRequiredException();
        }
        if (!userManager.isAdmin(userKey)) {
            throw new AuthorisationException("Client must be authenticated as an administrator to access this resource.");
        }
        return containerRequest;
    }
}

