package com.atlassian.plugins.rest.module.security;

import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.atlassian.plugins.rest.module.servlet.ServletUtils;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;

import java.security.Principal;

import static java.util.Objects.requireNonNull;

/**
 * SAL implementation of the {@link AuthenticationContext}
 *
 * @since 1.0
 */
public class SalAuthenticationContext implements AuthenticationContext {
    private final UserManager userManager;

    public SalAuthenticationContext(final UserManager userManager) {
        this.userManager = requireNonNull(userManager);
    }

    public Principal getPrincipal() {
        final UserProfile userProfile = getUserProfile();
        return userProfile != null ? new SalPrincipal(userProfile) : null;
    }

    public boolean isAuthenticated() {
        return getUserProfile() != null;
    }

    private UserProfile getUserProfile() {
        return userManager.getRemoteUser(ServletUtils.getHttpServletRequest());
    }

    private static class SalPrincipal implements Principal {
        private final UserProfile userProfile;

        SalPrincipal(UserProfile userProfile) {
            this.userProfile = requireNonNull(userProfile, "userProfile");
        }

        public String getName() {
            return userProfile.getUsername();
        }

        @Override
        public int hashCode() {
            return userProfile.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return (obj instanceof SalPrincipal) && ((SalPrincipal) obj).userProfile.equals(userProfile);
        }

        @Override
        public String toString() {
            return userProfile.getUsername();
        }
    }
}
