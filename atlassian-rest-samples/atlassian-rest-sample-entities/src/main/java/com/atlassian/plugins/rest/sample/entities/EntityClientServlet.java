package com.atlassian.plugins.rest.sample.entities;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.io.PrintWriter;

public class EntityClientServlet extends HttpServlet {
    private final RequestFactory restRequestFactory;
    private final ApplicationProperties applicationProperties;

    public static final String P_ORANGE = "orange";
    public static final String P_CONTENT_TYPE = "contentType";
    public static final String P_ACCEPT = "accept";

    public EntityClientServlet(RequestFactory restRequestFactory, ApplicationProperties applicationProperties) {
        this.restRequestFactory = restRequestFactory;
        this.applicationProperties = applicationProperties;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/plain");
        final PrintWriter out = resp.getWriter();
        try {

            String name = req.getParameter(P_ORANGE);
            if (name == null) {
                out.write("name is a required parameter");
                return;
            }

            Request restRequest = restRequestFactory.createRequest(Request.MethodType.POST, applicationProperties.getBaseUrl() + "/rest/entity/1/fruit");

            // leave Content-Type & Accept null if not otherwise specified
            String contentType = req.getParameter(P_CONTENT_TYPE);
            if (contentType != null) {
                restRequest.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
            }
            String[] accepts = req.getParameterValues(P_ACCEPT);
            if (accepts != null) {
                for (String accept : accepts) {
                    restRequest.addHeader(HttpHeaders.ACCEPT, accept);
                }
            }

            restRequest.setEntity(new Orange(name));

            restRequest.execute(new ResponseHandler<Response>() {
                public void handle(final Response restResponse) throws ResponseException {
                    Apple apple = restResponse.getEntity(Apple.class);
                    out.write(apple.getName() + "\n");
                    out.write("Content-Type=" + apple.getReqContentType() + "\n");
                    out.write("Accept=" + apple.getReqAccept());
                }
            });
            return;
        } catch (ResponseException e) {
            out.write(e.toString());
        } catch (RuntimeException e) {
            out.write(e.toString());
            throw e;
        } finally {
            out.close();
        }
    }

}
