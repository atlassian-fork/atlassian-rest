package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * JSON-marshalling version of JsonType
 *
 * @since v5.0
 */
@SuppressWarnings("UnusedDeclaration")
public class JsonTypeBean {
    @JsonProperty
    private String type;
    @JsonProperty
    private String items;
    @JsonProperty
    private String system;
    @JsonProperty
    private String custom;
    @JsonProperty
    private Long customId;

    public JsonTypeBean() {
    }

    public JsonTypeBean(String type, String items, String system, String custom, Long customId) {
        this.type = type;
        this.items = items;
        this.system = system;
        this.custom = custom;
        this.customId = customId;
    }

    public String getType() {
        return type;
    }

    public String getItems() {
        return items;
    }

    public String getSystem() {
        return system;
    }

    public String getCustom() {
        return custom;
    }

    public Long getCustomId() {
        return customId;
    }
}
