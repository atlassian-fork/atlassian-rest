package com.atlassian.plugins.rest.common.multipart;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Suppliers;
import com.google.common.base.Supplier;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.Serializable;

/**
 * Exception indicating the file size limit was exceeded
 *
 * @since 2.4
 */
public class FileSizeLimitExceededException extends WebApplicationException {
    private final static int PAYLOAD_TOO_LARGE = 413;
    private final static int NOT_FOUND = Response.Status.NOT_FOUND.getStatusCode();
    public final static String LEGACY_MODE_KEY =
            "atlassian.rest.filesize.exceeded.statuscode.legacy.enabled";

    @VisibleForTesting
    @Deprecated
    final static Supplier<Boolean> legacyMode =
            Suppliers.memoize(new LegacyModeSupplier());

    private static int getStatusCode() {
        return legacyMode.get() ? NOT_FOUND : PAYLOAD_TOO_LARGE;
    }

    public FileSizeLimitExceededException(String message) {
        super(Response.status(getStatusCode()).entity(message).build());
    }

    private static final class LegacyModeSupplier implements Supplier<Boolean>, Serializable {
        @Override
        public Boolean get() {
            return Boolean.getBoolean(LEGACY_MODE_KEY);
        }
    }
}
