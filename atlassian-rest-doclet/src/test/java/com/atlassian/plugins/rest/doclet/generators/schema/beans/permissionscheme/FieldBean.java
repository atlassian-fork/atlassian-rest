package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import com.atlassian.plugins.rest.doclet.generators.schema.beans.issue.JsonTypeBean;

import java.util.Set;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @since v5.0
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement (name = "field")
public class FieldBean
{

    @XmlElement
    private String id;

    @XmlElement
    private String name;

    @XmlElement
    private Boolean custom;

    @XmlElement
    private Boolean orderable;

    @XmlElement
    private Boolean navigable;

    @XmlElement
    private Boolean searchable;

    @XmlElement
    private Set<String> clauseNames;

    @XmlElement
    private JsonTypeBean schema;
}
