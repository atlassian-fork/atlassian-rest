package com.atlassian.plugins.rest.common.interceptor.impl;

import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;
import com.atlassian.plugins.rest.common.interceptor.impl.test.ClassResource;
import com.atlassian.plugins.rest.common.interceptor.impl.test.MethodResource;
import com.atlassian.plugins.rest.common.interceptor.impl.test.MyInterceptor;
import com.atlassian.plugins.rest.common.interceptor.impl.test.PackageResource;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InterceptorChainBuilderTest {
    @Test
    public void testMethodChain() throws NoSuchMethodException {
        verifyChain(MethodResource.class);
    }

    @Test
    public void testClassChain() throws NoSuchMethodException {
        verifyChain(ClassResource.class);
    }

    @Test
    public void testPackageChain() throws NoSuchMethodException {
        verifyChain(PackageResource.class);
    }

    @Test
    public void testDefaultChain() throws NoSuchMethodException {
        ContainerManagedPlugin plugin = mock(ContainerManagedPlugin.class);
        InterceptorChainBuilder builder = new InterceptorChainBuilder(plugin);
        List<ResourceInterceptor> interceptors = builder.getResourceInterceptorsForMethod(Object.class.getMethod("hashCode"));
        assertNotNull(interceptors);
        assertEquals(0, interceptors.size());
    }

    private void verifyChain(Class resourceClass)
            throws NoSuchMethodException {
        ContainerAccessor containerAccessor = mock(ContainerAccessor.class);
        when(containerAccessor.createBean(MyInterceptor.class)).thenReturn(new MyInterceptor());
        ContainerManagedPlugin plugin = mock(ContainerManagedPlugin.class);
        when(plugin.getContainerAccessor()).thenReturn(containerAccessor);
        InterceptorChainBuilder builder = new InterceptorChainBuilder(plugin);
        List<ResourceInterceptor> interceptors = builder.getResourceInterceptorsForMethod(resourceClass.getMethod("run"));
        assertNotNull(interceptors);
        assertEquals(1, interceptors.size());
        assertTrue("Interceptor was " + interceptors.get(0), interceptors.get(0) instanceof MyInterceptor);
    }


}
