package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A JSON-convertable representation of a Status
 *
 * @since v5.0
 */
@JsonIgnoreProperties (ignoreUnknown = true)
public class StatusJsonBean
{
    @JsonProperty
    private String self;

    @JsonProperty
    private String statusColor;

    @JsonProperty
    private String description;

    @JsonProperty
    private String iconUrl;

    @JsonProperty
    private String name;

    @JsonProperty
    private String id;

    @JsonProperty
    private StatusCategoryJsonBean statusCategory;
}
