package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import com.atlassian.plugins.rest.doclet.generators.schema.beans.issue.UserJsonBean;
import com.atlassian.rest.annotation.RestProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.util.Objects;

public final class PermissionHolderBean
{
    public static PermissionHolderBean holderBean(String type, String parameter)
    {
        return new PermissionHolderBean(type, parameter, null, null, null, null);
    }

    @JsonProperty (required = true)
    private String type;
    @JsonProperty (required = true)
    private String parameter;

    @JsonProperty
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    private UserJsonBean user;
    @JsonProperty
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    private GroupJsonBean group;
    @JsonProperty
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    private FieldBean field;
    @JsonProperty
    @RestProperty (scope = RestProperty.Scope.RESPONSE)
    private ProjectRoleBean projectRole;

    private boolean isInput = false;

    @Deprecated
    public PermissionHolderBean() {}

    private PermissionHolderBean(String type, String parameter, UserJsonBean user, GroupJsonBean group, FieldBean field, ProjectRoleBean projectRole)
    {
        this.type = type;
        this.parameter = parameter;
        this.user = user;
        this.group = group;
        this.field = field;
        this.projectRole = projectRole;
    }

    @JsonProperty
    public String getExpand()
    {
        return isInput ? null : "user,group,field,projectRole";
    }

    public PermissionHolderBean input()
    {
        this.isInput = true;
        return this;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getParameter()
    {
        return parameter;
    }

    public void setParameter(String parameter)
    {
        this.parameter = parameter;
    }

    public UserJsonBean getUser()
    {
        return user;
    }

    public void setUser(UserJsonBean user)
    {
        this.user = user;
    }

    public GroupJsonBean getGroup()
    {
        return group;
    }

    public void setGroup(GroupJsonBean group)
    {
        this.group = group;
    }

    public FieldBean getField()
    {
        return field;
    }

    public void setField(FieldBean field)
    {
        this.field = field;
    }

    public ProjectRoleBean getProjectRole()
    {
        return projectRole;
    }

    public void setProjectRole(ProjectRoleBean projectRole)
    {
        this.projectRole = projectRole;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static Builder builder(PermissionHolderBean data)
    {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        PermissionHolderBean that = (PermissionHolderBean) o;
        
        return Objects.equals(this.type, that.type) &&
                Objects.equals(this.parameter, that.parameter) &&
                Objects.equals(this.user, that.user) &&
                Objects.equals(this.group, that.group) &&
                Objects.equals(this.field, that.field) &&
                Objects.equals(this.projectRole, that.projectRole);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(type, parameter, user, group, field, projectRole);
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("parameter", parameter)
                .add("user", user)
                .add("group", group)
                .add("field", field)
                .add("projectRole", projectRole)
                .toString();
    }

    public static final class Builder
    {

        private String type;
        private String parameter;
        private UserJsonBean user;
        private GroupJsonBean group;
        private FieldBean field;
        private ProjectRoleBean projectRole;

        private Builder() {}

        private Builder(PermissionHolderBean initialData)
        {

            this.type = initialData.type;
            this.parameter = initialData.parameter;
            this.user = initialData.user;
            this.group = initialData.group;
            this.field = initialData.field;
            this.projectRole = initialData.projectRole;
        }

        public Builder setType(String type)
        {
            this.type = type;
            return this;
        }

        public Builder setParameter(String parameter)
        {
            this.parameter = parameter;
            return this;
        }

        public Builder setUser(UserJsonBean user)
        {
            this.user = user;
            return this;
        }

        public Builder setGroup(GroupJsonBean group)
        {
            this.group = group;
            return this;
        }

        public Builder setField(FieldBean field)
        {
            this.field = field;
            return this;
        }

        public Builder setProjectRole(ProjectRoleBean projectRole)
        {
            this.projectRole = projectRole;
            return this;
        }

        public PermissionHolderBean build()
        {
            return new PermissionHolderBean(type, parameter, user, group, field, projectRole);
        }
    }
}
