package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import java.net.URI;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @since v4.4
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement (name = "projectRoleActor")
public class RoleActorBean
{
    @XmlElement
    Long id;

    @XmlElement
    String displayName;

    @XmlElement
    String type;

    @XmlElement
    String name;

    @XmlElement
    URI avatarUrl;
}
