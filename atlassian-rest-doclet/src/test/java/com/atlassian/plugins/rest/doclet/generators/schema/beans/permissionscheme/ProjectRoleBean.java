package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissionscheme;

import java.net.URI;
import java.util.Collection;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @since v4.4
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement (name = "projectRole")
public class ProjectRoleBean
{
    @XmlElement
    public URI self;

    @XmlElement
    public String name;

    @XmlElement
    public Long id;

    @XmlElement
    public String description;

    @XmlElement
    public Collection<RoleActorBean> actors;
}
